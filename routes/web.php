<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('project', 'ProjectController');
Route::resource('user', 'UserController');
Route::resource('totaltime', 'TotalTimeController');
Route::resource('projecttime', 'ProjectTimeController');
//Les routes d'authentification
Auth::routes();
//Route accueil
Route::get('/', 'HomeController@index')
    ->name('home');
//Route de la page principale admin avec le middleware qui vérifie que le user est bien admin
Route::get('/admin', 'AdminController@admin')    
    ->middleware('is_admin')    
    ->name('admin');
//Route groupé avec préfix admin    
Route::group(['prefix'=>'admin'], function() {
    Route::get('/ajouter-client', 'ClientController@create')->name('client.create');
    Route::get('/clients', 'ClientController@index')->name('client.list');
    Route::get('/client/{id}', 'ClientController@show')->name('client.show');
    Route::post('/clients', 'ClientController@store')->name('client.store');
    Route::get('/ajouter-projet', 'ProjectController@create')->name('project.create');
    Route::get('/projets', 'ProjectController@index')->name('project.list');
    Route::get('/projet/{id}', 'ProjectController@show')->name('project.show');
    Route::get('/utilisateurs', 'UserController@index')->name('user.list');
    Route::get('/ajouter-utilisateur', 'UserController@create')->name('user.create');
    Route::get('/utilisateur/{id}', 'UserController@show')->name('user.show');
    Route::post('/utilisateurs', 'UserController@store')->name('user.store');
    Route::post('/projet', 'CrossController@store')->name('cross.store');
});
//Route de déconnexion 
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
