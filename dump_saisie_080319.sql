-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.24 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Listage de la structure de la table saisie-des-temps. clients
DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table saisie-des-temps.clients : ~2 rows (environ)
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
REPLACE INTO `clients` (`id`, `created_at`, `updated_at`, `name`) VALUES
	(14, '2019-03-07 15:37:03', '2019-03-07 15:37:03', 'Client n°1'),
	(15, '2019-03-07 15:37:12', '2019-03-07 15:37:12', 'Client n°2'),
	(16, '2019-03-07 15:37:20', '2019-03-07 15:37:20', 'Client n°3');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;

-- Listage de la structure de la table saisie-des-temps. migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table saisie-des-temps.migrations : ~6 rows (environ)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
REPLACE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_03_06_153804_create_clients_table', 1),
	(4, '2019_03_06_153804_create_project_times_table', 1),
	(5, '2019_03_06_153804_create_projects_table', 1),
	(6, '2019_03_06_153804_create_total_times_table', 1),
	(7, '2019_03_06_153814_create_foreign_keys', 2),
	(8, '2016_01_04_173148_create_admin_tables', 3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Listage de la structure de la table saisie-des-temps. password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table saisie-des-temps.password_resets : ~0 rows (environ)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Listage de la structure de la table saisie-des-temps. projects
DROP TABLE IF EXISTS `projects`;
CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clients_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_clients_id_foreign` (`clients_id`),
  CONSTRAINT `projects_clients_id_foreign` FOREIGN KEY (`clients_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table saisie-des-temps.projects : ~4 rows (environ)
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
REPLACE INTO `projects` (`id`, `created_at`, `updated_at`, `name`, `clients_id`) VALUES
	(3, '2019-03-07 15:37:33', '2019-03-07 15:37:33', 'Projet important', 14),
	(4, '2019-03-07 15:37:42', '2019-03-07 15:37:42', 'Projet secondaire', 14),
	(5, '2019-03-07 15:37:51', '2019-03-07 15:37:51', 'Projet en cours', 15),
	(6, '2019-03-07 15:38:00', '2019-03-07 15:38:00', 'A revoir', 15),
	(7, '2019-03-07 15:38:16', '2019-03-07 15:38:16', 'A terminer rapidement', 15);
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;

-- Listage de la structure de la table saisie-des-temps. project_times
DROP TABLE IF EXISTS `project_times`;
CREATE TABLE IF NOT EXISTS `project_times` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `projects_id` int(10) unsigned NOT NULL,
  `time` int(11) NOT NULL,
  `users_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_times_projects_id_foreign` (`projects_id`),
  KEY `project_times_users_id_foreign` (`users_id`),
  CONSTRAINT `project_times_projects_id_foreign` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  CONSTRAINT `project_times_users_id_foreign` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table saisie-des-temps.project_times : ~0 rows (environ)
/*!40000 ALTER TABLE `project_times` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_times` ENABLE KEYS */;

-- Listage de la structure de la table saisie-des-temps. total_times
DROP TABLE IF EXISTS `total_times`;
CREATE TABLE IF NOT EXISTS `total_times` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `time` int(11) NOT NULL,
  `clients_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `total_times_clients_id_foreign` (`clients_id`),
  CONSTRAINT `total_times_clients_id_foreign` FOREIGN KEY (`clients_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table saisie-des-temps.total_times : ~0 rows (environ)
/*!40000 ALTER TABLE `total_times` DISABLE KEYS */;
/*!40000 ALTER TABLE `total_times` ENABLE KEYS */;

-- Listage de la structure de la table saisie-des-temps. users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'default',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table saisie-des-temps.users : ~1 rows (environ)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `type`) VALUES
	(1, 'Sinjy', 'kevin.sinjy@applilogik.fr', NULL, '$2y$10$FutBUp6cDP9Q9B5xaqrM/e6MJnp4cnf8F935ajFjfRblgM9o0jvM6', NULL, '2019-03-07 08:48:02', '2019-03-07 10:03:02', 'admin'),
	(2, 'bernard', 'bernard@bernard.fr', NULL, '$2y$10$HuSIroR9GY0GB1NBzAtsZu0SuCxl6UbwYSk41rvFKbg5s.TSMoqPy', NULL, '2019-03-07 14:46:45', '2019-03-07 14:46:45', 'default');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
