<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTotalTimesTable extends Migration {

	public function up()
	{
		Schema::create('total_times', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('time');
			$table->integer('clients_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('total_times');
	}
}