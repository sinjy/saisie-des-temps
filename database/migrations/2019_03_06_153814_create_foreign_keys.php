<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('projects', function(Blueprint $table) {
			$table->foreign('clients_id')->references('id')->on('clients')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('cross', function(Blueprint $table) {
			$table->foreign('users_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('restrict');
			$table->foreign('projects_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('total_times', function(Blueprint $table) {
			$table->foreign('clients_id')->references('id')->on('clients')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('project_times', function(Blueprint $table) {
			$table->foreign('projects_id')->references('id')->on('projects')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('project_times', function(Blueprint $table) {
			$table->foreign('users_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
	}

	public function down()
	{
		Schema::table('projects', function(Blueprint $table) {
			$table->dropForeign('projects_clients_id_foreign');
		});
		Schema::table('total_times', function(Blueprint $table) {
			$table->dropForeign('total_times_clients_id_foreign');
		});
		Schema::table('project_times', function(Blueprint $table) {
			$table->dropForeign('project_times_projects_id_foreign');
		});
		Schema::table('project_times', function(Blueprint $table) {
			$table->dropForeign('project_times_users_id_foreign');
		});
	}
}