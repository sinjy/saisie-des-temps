<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrossTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cross', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('users_id')->unsigned();
            $table->integer('projects_id')->unsigned();
            $table->string('users_name');
            $table->string('projects_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cross');
    }
}
