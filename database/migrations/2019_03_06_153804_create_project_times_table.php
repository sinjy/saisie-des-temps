<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectTimesTable extends Migration {

	public function up()
	{
		Schema::create('project_times', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('projects_id')->unsigned();
			$table->integer('time');
			$table->integer('users_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('project_times');
	}
}