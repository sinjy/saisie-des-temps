<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller 
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    //On récupère en DB les colonnes 'id' et 'name' de la colonne client
    $clients = DB::table('clients')->select('id', 'name')->get();
    return view('client.index')->with('clients', $clients);

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    return view('client.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $request->validate([
      'name'=>'required'
  ]);
  //Nouveau client
  $client = new Client([
      'name'=> $request->get('name')
  ]);
  $client->save();
  return redirect('/admin/clients')->with('success', 'Client ajouté !');

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    //On récupère les colonnes 'id', 'name', 'clients_id' de la table projects 
    $projects = DB::table('projects')->select('id', 'name', 'clients_id')->get();
    //On récupère les $projects où clients_id est égal à $id
    $project = $projects->where('clients_id', $id);
    //On récupère les colonnes 'id' et 'name' de la table clients
    $clients = DB::table('clients')->select('id', 'name')->get();
    //On récupère les $clients où l'id est égale à $id
    $client = $clients->where('id', $id);
    return view('client.show', ['client' => Client::findOrFail($id)], compact('project', $project, 'client', $client));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
  
}

?>