<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cross;
use Illuminate\Support\Facades\DB;

class CrossController extends Controller
{

    /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    //On récupère l'id de la route
    $id = $request->route('id');
    $request->validate([
      'users_id'=>'required',
      'projects_id'=>'required'
    ]);
    //Nouveau 'pivot'
    $cross = new Cross([
        'users_id'=> $request->get('users_id'),
        'projects_id'=>$request->get('projects_id'),
        'users_name' => $request->get('users_name'),
        'projects_name' => $request->get('projects_name')
    ]);
    $cross->save();
    //Tentative de jointure de table
    DB::table('cross')
        ->leftJoin('users_name', 'users.name');
        
     return back()->with('success', 'Utilisateur assigné', 'id', $id);

  }
}
