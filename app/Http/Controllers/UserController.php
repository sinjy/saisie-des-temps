<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller 
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    //On récupère 'id' et 'name' de la table 'users'
    $users = DB::table('users')->select('id', 'name')->get();
    return view('user.index')->with('users', $users);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    return view('user.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $request->validate([
      'name'=>'required',
      'email'=>'required',
      'password'=>'required'
    ]);
    //Nouveau User
    $user = new User([
      'name' => $request->get('name'),
      'email' => $request->get('email'),
      'password' => Hash::make($request['password']),
      'type' => $request->get('type'),        

    ]);
    $user->save();
    return redirect('/admin/utilisateurs')->with('success', 'Utilisateur ajouté');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    //On récupère en DB 'id' et 'name' de la table 'users'
    $users = DB::table('users')->select('id', 'name')->get();
    //On récupère les 'id' et 'name' du user où l'id === $id
    $user = $users->where('id', $id);
    //On récupère 'users_id' et 'projects_id' de la table cross en DB
    $projects= DB::table('cross')->select('users_id','projects_id')->get();
    //On récupère les $projects où users_id est égale à $id
    $project = $projects->where('users_id', $id);
    //On retourne la vue user.show en tentant de récupérer son id et en passant les variables $user et $project dans la view
    return view('user.show', ['user' => User::findOrFail($id)], compact('user', $user, 'project', $project));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
  
}

?>