<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Project;
class ProjectController extends Controller 
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    //On récupère l'id et le name de la table projects
    $projects = DB::table('projects')->select('id', 'name')->get();
    return view('project.index')->with('projects', $projects);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    //On récupère 'id' et 'name' de la table 'clients'
    $clients = DB::table('clients')->select('id', 'name')->get();
    return view('project.create')->with('clients', $clients);

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $request->validate([
      'name'=>'required',
      'client_name'=>'required'
  ]);
  //Nouveau Project
  $project = new Project([
      'name'=> $request->get('name'),
      'clients_id'=>$request->get('client_name')
  ]);
  $project->save();
  return redirect('/admin/projets')->with('success', 'projet ajouté !');

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    //On récupère tout de la table cross
    $usersCross = DB::table('cross')->select('*')->get();
    //On récupère 'id' et 'name' de la table 'user'
    $users = DB::table('users')->select('id', 'name')->get();
    //On récupère 'id' et 'name' de la table 'projects'
    $projects = DB::table('projects')->select('id', 'name')->get();
    //On récupère $projects où l'id est égal à $id
    $project = $projects->where('id', $id);
    return view('project.show', ['project' => Project::findOrFail($id)], compact('project', $project, 'usersCross', $usersCross, 'users', $users));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
  
}

?>