<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cross extends Model
{
    protected $table = 'cross';
    public $timestamps = true;
    protected $fillable = ['users_id', 'projects_id', 'users_name', 'projects_name'];
}
