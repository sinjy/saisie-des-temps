<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectTime extends Model 
{

    protected $table = 'project_times';
    public $timestamps = true;

}