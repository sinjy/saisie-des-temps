<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TotalTime extends Model 
{

    protected $table = 'total_times';
    public $timestamps = true;

}