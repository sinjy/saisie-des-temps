@extends('layout')
@section('content')
<div class="card card-image my-5" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/gradient1.jpg);">
    <div class="text-white text-center py-5 px-4 my-5">
      <div>
        <h2 class="card-title h1-responsive pt-3 mb-5 font-bold"><strong>Bienvenue</strong></h2>
        <p class="mx-5 mb-5">Gérez votre temps et découvrez le temps que vous passé réellement par projet.</p>
        <a class="btn btn-outline-white btn-md" href="#"><i class="fas fa-hourglass-start"></i> Commencer</a>
        <a class="btn btn-outline-white btn-md" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt"></i> Logout</a>
        <!--Vérification si user = admin alors afficher le lien du panel admin -->
        @if (auth()->check())
          @if (auth()->user()->isAdmin())
            <a class="btn btn-outline-white btn-md" href="{{ route('admin') }}"><i class="fas fa-unlock-alt"></i> Admin Panel</a>          
          @endif
        @endif
      </div>
    </div>
  </div>
@endsection