@extends('layout')
@section('content')
<div class="jumbotron jumbotron-fluid">
    <!-- retour à la page admin -->
    <a href="{{ URL('/admin') }}">
        <i class="fas fa-arrow-left"></i> Admin panel
    </a>
    <div class="container">
        <h1>Liste des utlisateurs</h1>
        @forelse ($users as $user)
            <ul>
                <li><a href="{{ URL('/admin/utilisateur/' .$user->id) }}">{{ $user->name}}</a></li>
            </ul>
        @empty
        Pas d'utilisateur'.    
        @endforelse
        <a class="btn btn-primary" href="{{URL('/admin/ajouter-utilisateur')}}">Ajouter un utilisateur</a>
    </div>
</div>
@endsection