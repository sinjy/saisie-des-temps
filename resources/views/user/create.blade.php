@extends('layout')
@section('content')
<form action="{{ route('user.store') }}" method="POST" class="text-center border border-light p-5 my-5">
    @csrf
    <p class="h4 mb-4">Ajouter un utilisateur</p>
    <input type="text" class="form-control mb-4" placeholder="Nom de l'utilisateur" name="name">
    <input id="email" type="email" placeholder="email" class="mb-4 form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" required>
        {{--Vérification d'e-mail--}}
        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
        <input id="password" type="password" class="mb-4 form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="mot de passe" required>
        @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif        
        <select class="form-control mb-4">
            <option value="default" name="type">Defaut</option>
            <option value="admin" name="type">Admin</option>
        </select>
    <button class="btn btn-info btn-block my-4" type="submit">Ajouter</button>
</form>
@endsection