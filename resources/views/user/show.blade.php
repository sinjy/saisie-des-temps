@extends('layout')
@section('content')
<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <!--retour à la liste de users-->
        <a href="{{ URL('/admin/utilisateurs') }}">
            <i class="fas fa-arrow-left"></i> Liste des utlisateurs
        </a> 
        <h1 class="text-align-center">Nom de l'utilisateur : {{ $user->name }}</h1>
        <h2>Projets :</h2>
        @forelse ($project as $project)
        <ul>
            <li>{{$project->name}}</li>
        </ul>
        @empty
        Pas de projet en cours.
        @endforelse
    </div>
</div>
@endsection