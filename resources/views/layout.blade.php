<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Saisie des temps</title>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.4/css/mdb.min.css" rel="stylesheet">
  <link rel="manifest" href="manifest.json">
</head>
<body>
  <div class="container">
    @yield('content')
  </div>
  <div id="block" style="display:none;">
    <button id="on-activation-request">
  </div>
  <script src="{{ asset('js/app.js') }}" type="text/js"></script>
  <!-- JQuery -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.4/js/mdb.min.js"></script>
  <script>
    if ("serviceWorker" in navigator) {
    // On essaye d'enregistrer le service
    // worker
    navigator.serviceWorker
    .register("../public/service-worker.js")
    .then(registration => {
      // Le Service Worker a fini d'être
      // téléchargé.
      console.log(
        "App: Téléchargement fini."
      );

      // (1) A chaque fois qu'il y a une
      // mise à jour des Service Workers
      registration.addEventListener(
        "updatefound",
        () => {
          // (1) On récupère le Service
          // Worker en cours
          // d'installation
          const newWorker =
            registration.installing;
          // `registration` a aussi
          // les clés `active` et 
          // `waiting` qui permettent
          // de récupérer les Service
          // Workers correspondant

          // (2) A chaque fois que le
          // Service Worker en cours
          // d'installation change
          // de statut
          newWorker.addEventListener(
            "statechange",
            () => {
              // (2) On affiche son
              // nouveau statut
              console.log(
                "App: Nouvel état :",
                newWorker.state
              );
            }
          );
        }
      );
    })
    .catch(err => {
      // Il y a eu un problème
      console.error(
        "App: Crash de Service Worker",
        err
      );
    });
}

function displayNotification() {
  document
    .querySelector("#notification")
    .style.display = "block";
}

navigator.serviceWorker
  .getRegistration()
  .then(registration => {
    registration.addEventListener(
      "updatefound",
      () => {
        // On récupère le Service
        // Worker en cours
        // d'installation
        const newWorker =
          registration.installing;

        // On se branche à ses mises
        // à jour pour savoir quand
        // il a fini de s'installer
        newWorker.addEventListener(
          "statechange",
          () => {
            if (newWorker.state ===
                "installed") {
              // Un nouveau Service
              // Worker est prêt.
              // Donc on affiche la
              // notification
              displayNotification();
            }
          }
        );
      }
    );
  });

  // Au clic du bouton de notification
document.querySelector("#on-activation-request")
  .addEventListener("click", () => {
    // On récupère le Service Worker
    // qui a fini de s'installer
    // (waiting)
    navigator.serviceWorker
      .getRegistration()
      .then(registration => {
        // Et on lui envoie le
        // message d'activation
        registration.waiting
          .postMessage("skipWaiting");
      });
  });
  navigator.serviceWorker
  .addEventListener(
    "controllerchange",
    () => {
      // Ici, on rafraîchit la page
      // pour repartir à zéro.
      window.location.reload();
    }
  );
</script>
</body>
</html>