@extends('layout')
@section('content')
<style>
    .fa-1x {
font-size: 1.5rem;
}
.navbar-toggler.toggler-example {
cursor: pointer;
}
.dark-blue-text {
color: #0A38F5;
}
.dark-pink-text {
color: #AC003A;
}
.dark-amber-text {
color: #ff6f00;
}
.dark-teal-text {
color: #004d40;
}
</style>
<!--Navbar-->
<nav class="navbar navbar-light light-blue lighten-4">
  <!-- Navbar brand -->
  <a class="navbar-brand" href="#">Admin Panel</a>
  <!-- Collapse button -->
  <button class="navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1"
    aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation"><span class="dark-blue-text"><i
        class="fas fa-bars fa-1x"></i></span></button>
  <!-- Collapsible content -->
  <div class="collapse navbar-collapse" id="navbarSupportedContent1">
    <!-- Links -->
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="{{route('admin')}}">Tableau de bord <i class="fas fa-tachometer-alt"></i> <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('client.list')}}">Clients</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('user.list') }}">Utilisateurs</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('project.list')}}">Projets</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Récapitulatif</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('home')}}">Lancer un projet</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('logout')}}">Logout</a>
      </li>
    </ul>
    <!-- Links -->
  </div>
  <!-- Collapsible content -->
</nav>
<!--/.Navbar-->
@endsection