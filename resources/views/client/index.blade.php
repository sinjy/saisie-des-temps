@extends('layout')
@section('content')

<div class="jumbotron jumbotron-fluid">
    {{--retour à la page principal admin --}}
        <a href="{{ URL('/admin') }}">
            <i class="fas fa-arrow-left"></i> Admin panel
        </a>    
    <div class="container">
        <h1>Liste des clients</h1>
        @forelse ($clients as $client)
            <ul>
                <li><a href="{{ URL('/admin/client/' .$client->id) }}">{{ $client->name}}</a></li>
            </ul>
        @empty
        Pas de client.    
        @endforelse
        <a class="btn btn-primary" href="{{URL('/admin/ajouter-client')}}">Ajouter un client</a>
    </div>
</div>
@endsection