@extends('layout')
@section('content')
<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <a href="{{ URL('/admin/clients') }}">
        <i class="fas fa-arrow-left"></i> Liste des clients
        </a> 
        <h1 class="text-align-center">Nom du client : {{ $client->name }}</h1>
        <h2>Projets :</h2>
        @forelse ($project as $project)
        <ul>
            <li>{{$project->name}}</li>
        </ul>
        @empty
        Pas de projet en cours.
        @endforelse
    </div>
</div>
@endsection