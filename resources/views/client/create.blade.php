@extends('layout')
@section('content')
{{--formulaire d'ajout d'un client --}}
<form action="{{ route('client.store') }}" method="POST" class="text-center border border-light p-5 my-5">
    @csrf
    <p class="h4 mb-4">Ajouter un client</p>

    <!-- name -->
    <input type="text" class="form-control mb-4" placeholder="Nom du client" name="name">
    <button class="btn btn-info btn-block my-4" type="submit">Ajouter</button>
</form>

@endsection