@extends('layout')
@section('content')
<form action="{{ route('project.store') }}" method="POST" class="text-center border border-light p-5 my-5">
    @csrf
    <p class="h4 mb-4">Ajouter un projet</p>
    <input type="text" class="form-control mb-4" placeholder="Nom du projet" name="name">
    <select name="client_name">
    @forelse ($clients as $client)
        <option value="{{ $client->id}}">{{ $client->name}}</option>
    @empty 
    Créez un client
    @endforelse
    </select>    
    <button class="btn btn-info btn-block my-4" type="submit">Ajouter</button>
</form>

@endsection