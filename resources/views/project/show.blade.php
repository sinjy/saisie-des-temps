@extends('layout')
@section('content')
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <a href="{{ URL('/admin/projets') }}">
    <i class="fas fa-arrow-left"></i> Liste des projets
    </a> 
    <h1 class="text-align-center">Nom du projet : {{ $project->name }}</h1>
    <h2>Utilisateurs :</h2>
    @forelse ($usersCross as $user)
    <ul>
      <li>{{$user->users_name}}</li>
    </ul>
    @empty
    Personne n'a entamé ce projet.
    @endforelse
    <!--Modal d'ajout d'utilisateur sur un projet -->
    <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header text-center">
            <h4 class="modal-title w-100 font-weight-bold">Assigner ce projet à des utilisateurs</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body mx-3">
            <form action="{{ route('cross.store') }}" method="POST" class="text-center border border-light p-5 my-5">
              @csrf 
              <input type="hidden"  name="projects_id" value="{{$project->id}}">
              {{--Variable inputId afin d'incrémenter l'id de la modal = defaultUnchecked et for = defaultUnchecked --}}
              <?php 
              $inputId= 1;
              $labelId= 1;
              ?>
              @forelse ($users as $user)
              <div class="md-form mb-5">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="defaultUnchecked<?php echo ++$inputId; ?>" name="users_id" value="{{$user->id}}">
                  <label class="custom-control-label" for="defaultUnchecked<?php echo ++$labelId; ?>">{{ $user->name }}</label>
                </div>
              </div>
              @empty
                Il n'y a pas d'utilisateur.
              @endforelse
              <input type="hidden" value="{{$project->name}}" name="projects_name">
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-default" type="submit">Assigner</button>
                </div>
            </form>    
          </div>
        </div>
      </div>
    </div>    
    <div class="text-center">
      <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Assigner ce projet à des utilisateurs</a>
    </div>
  </div>
</div>
@endsection