@extends('layout')
@section('content')
    
<div class="jumbotron jumbotron-fluid">
    <a href="{{ URL('/admin') }}">
    <i class="fas fa-arrow-left"></i> Admin panel
    </a>
    <div class="container">
        <h1>Liste des projets</h1>
        @forelse ($projects as $project)
        <ul>
            <li><a href="{{ URL('/admin/projet/' .$project->id) }}">{{ $project->name}}</a></li>
        </ul>
        @empty
        Pas de projet.    
        @endforelse
        <a class="btn btn-primary" href="{{URL('/admin/ajouter-projet')}}">Ajouter un projet</a>
    </div>
</div>
@endsection