<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Saisie des temps') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="manifest" href="manifest.json">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Saisie des temps') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <div id="block" style="display:none;">
          <button id="on-activation-request">
        </div>
        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <script>
if ("serviceWorker" in navigator) {
  // On essaye d'enregistrer le service
  // worker
  navigator.serviceWorker
    .register("../public/service-worker.js")
    .then(registration => {
      // Le Service Worker a fini d'être
      // téléchargé.
      console.log(
        "App: Téléchargement fini."
      );

      // (1) A chaque fois qu'il y a une
      // mise à jour des Service Workers
      registration.addEventListener(
        "updatefound",
        () => {
          // (1) On récupère le Service
          // Worker en cours
          // d'installation
          const newWorker =
            registration.installing;
          // `registration` a aussi
          // les clés `active` et 
          // `waiting` qui permettent
          // de récupérer les Service
          // Workers correspondant

          // (2) A chaque fois que le
          // Service Worker en cours
          // d'installation change
          // de statut
          newWorker.addEventListener(
            "statechange",
            () => {
              // (2) On affiche son
              // nouveau statut
              console.log(
                "App: Nouvel état :",
                newWorker.state
              );
            }
          );
        }
      );
    })
    .catch(err => {
      // Il y a eu un problème
      console.error(
        "App: Crash de Service Worker",
        err
      );
    });
}

function displayNotification() {
  document
    .querySelector("#notification")
    .style.display = "block";
}

navigator.serviceWorker
  .getRegistration()
  .then(registration => {
    registration.addEventListener(
      "updatefound",
      () => {
        // On récupère le Service
        // Worker en cours
        // d'installation
        const newWorker =
          registration.installing;

        // On se branche à ses mises
        // à jour pour savoir quand
        // il a fini de s'installer
        newWorker.addEventListener(
          "statechange",
          () => {
            if (newWorker.state ===
                "installed") {
              // Un nouveau Service
              // Worker est prêt.
              // Donc on affiche la
              // notification
              displayNotification();
            }
          }
        );
      }
    );
  });

  // Au clic du bouton de notification
document.querySelector("#on-activation-request")
  .addEventListener("click", () => {
    // On récupère le Service Worker
    // qui a fini de s'installer
    // (waiting)
    navigator.serviceWorker
      .getRegistration()
      .then(registration => {
        // Et on lui envoie le
        // message d'activation
        registration.waiting
          .postMessage("skipWaiting");
      });
  });
  navigator.serviceWorker
  .addEventListener(
    "controllerchange",
    () => {
      // Ici, on rafraîchit la page
      // pour repartir à zéro.
      window.location.reload();
    }
  );
</script>
</body>
</html>
